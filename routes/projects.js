var express = require('express');
var router = express.Router();
var authentication_mdl = require('../middleware/authentication');
var session_store;
 
router.get('/', authentication_mdl.is_login, function(req, res, next) {
    req.getConnection(function(err, connection) {
        var query = connection.query('SELECT * FROM projects', function(err, rows) {
            if (err)
                var errorMsg = ("Error Selecting : %s ", err);   
            req.flash('msg_error', errorMsg);
            console.log(rows);
            res.render('project/list', {title: "Projects", data: rows});
        });
     });
});

router.post('/new', authentication_mdl.is_login, function(req, res, next) {
    req.assert('title', 'Title is required.').notEmpty();
    var errors = req.validationErrors();
    if (!errors) {
    	console.log('ok');
        var title = req.sanitize('title');
        var description = req.sanitize('description'); 
        var price = req.sanitize('price'); 
        if (price == '') price = null;
        var completed_work = req.sanitize('completed_work'); 
        var start_date = req.sanitize('start_date');
        if (start_date == '') start_date = null;
        var end_date = req.sanitize('end_date');
        if (end_date == '') end_date = null;
        var team_members = req.sanitize('team_members'); 
 
        var project = {
            title: title,
            description: description,
            price: price,
            completed_work : completed_work,
            start_date: start_date,
            end_date: end_date,
            user_id: req.session.user.id,
            team_members: team_members
        }
 	
 		console.log(req.param('start_date'));
        var insert_sql = 'INSERT INTO projects SET ?';
        req.getConnection(function(err, connection) {
            var query = connection.query(insert_sql, project, function(err, result) {
                if(err) {
                    var errors_detail  = ("Error Insert : %s ", err);   
                    req.flash('msg_error', errors_detail); 
                    res.render('project/create', { 
                        title: req.param('title'), 
                        description: req.param('description'),
                        price: req.param('price'),
                        completed_work: req.param('completed_work'),
                        start_date: req.param('start_date'),
                        end_date: req.param('end_date'),
                        team_members: req.param('team_members')
                    });
                }else{
                    req.flash('msg_info', 'Create project success'); 
                    res.redirect('/projects');
                }       
            });
        });
    } else {
        console.log('err', errors);

        req.flash('msg_error', errors[0].msg);
        res.render('project/create', { 
        	title: req.param('title'), 
            description: req.param('description'),
            price: req.param('price'),
            completed_work: req.param('completed_work'),
            start_date: req.param('start_date'),
            end_date: req.param('end_date'),
            team_members: req.param('team_members')
        });
    }
 
});
 
router.get('/new', authentication_mdl.is_login, function(req, res, next) {
    res.render('project/create', { 
        title: '', 
        description: '',
        price: '',
        completed_work: '',
        start_date: '',
        end_date: '',
        team_members: ''
    });
});

router.get('/edit/(:id)', authentication_mdl.is_login, function(req,res,next){
    req.getConnection(function(err,connection){
        var query = connection.query('SELECT * FROM projects where id='+req.params.id,function(err,rows)
        {
            if(err)
            {
                var errors_detail  = ("Error Selecting : %s ",err );  
                req.flash('msg_error', errors_detail); 
                res.redirect('/projects'); 
            }else
            {
                if(rows.length <=0)
                {
                    req.flash('msg_error', "Project can't be found!"); 
                    res.redirect('/projects');
                }
                else
                {   
                    console.log(rows);
                    res.render('project/edit',{data:rows[0]});
 
                }
            }
 
        });
    });
});
router.put('/edit/(:id)', authentication_mdl.is_login, function(req,res,next){
    req.assert('title', 'Title is required.').notEmpty();
    var errors = req.validationErrors();
    if (!errors) {
    	console.log('ok');
        var title = req.sanitize('title');
        var description = req.sanitize('description'); 
        var price = req.sanitize('price'); 
        if (price == '') price = null;
        var completed_work = req.sanitize('completed_work'); 
        var start_date = req.sanitize('start_date');
        if (start_date == '') start_date = null;
        var end_date = req.sanitize('end_date');
        if (end_date == '') end_date = null;
        var team_members = req.sanitize('team_members');
 
        var project = {
            title: title,
            description: description,
            price: price,
            completed_work : completed_work,
            start_date: start_date,
            end_date: end_date,
            team_members: team_members
        }
 
        var update_sql = 'update projects SET ? where id = '+req.params.id;
        req.getConnection(function(err,connection){
            var query = connection.query(update_sql, project, function(err, result){
                if(err)
                {
                    var errors_detail  = ("Error Update : %s ",err );   
                    req.flash('msg_error', errors_detail); 
                    res.render('project/edit', 
                    { 
                        title: req.param('title'), 
                        description: req.param('description'),
                        price: req.param('price'),
                        completed_work: req.param('completed_work'),
                        start_date: req.param('start_date'),
                        end_date: req.param('end_date'),
            			team_members: req.param('team_members')
                    });
                }else{
                    req.flash('msg_info', 'Update project success'); 
                    res.redirect('/projects/edit/'+req.params.id);
                }       
            });
        });
    }else{
        console.log('err', errors);

        req.flash('msg_error', errors[0].msg);
        res.redirect('/projects/edit/'+req.params.id);
    }
});

router.delete('/delete/(:id)', authentication_mdl.is_login, function(req, res, next) {
    req.getConnection(function(err,connection){
        var project = {
            id: req.params.id,
        }
         
        var delete_sql = 'delete from projects where ?';
        req.getConnection(function(err,connection){
            var query = connection.query(delete_sql, project, function(err, result){
                if(err)
                {
                    var errors_detail  = ("Error Delete : %s ",err);
                    req.flash('msg_error', errors_detail); 
                    res.redirect('/projects');
                }
                else{
                    req.flash('msg_info', 'Delete Project Success'); 
                    res.redirect('/projects');
                }
            });
        });
    });
});

module.exports = router;