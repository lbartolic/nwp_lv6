var express = require('express');
var router = express.Router();
 
var session_store;
router.get('/', function(req, res, next) {
    res.redirect('/projects');
});
router.get('/login',function(req,res,next){
	res.render('main/login', {title:"Login Page"});
});
router.post('/login',function(req,res,next){
    session_store=req.session;
    req.assert('txtEmail', 'Username is required.').notEmpty();
    req.assert('txtEmail', 'Email is not valid.').isEmail();
    req.assert('txtPassword', 'Password is required.').notEmpty();
    var errors = req.validationErrors();
    if (!errors) {
        req.getConnection(function(err,connection){
            var pw = req.sanitize( 'txtPassword' ); 
            var email = req.sanitize( 'txtEmail' );
             
            var query = connection.query('select * from users where email="'+email+'" and password=md5("'+pw+'")',function(err,rows) {
                if(err) {
                    var errorMsg  = ("Error Selecting : %s ", err.code );  
                    console.log(err.code);
                    req.flash('msg_error', errorMsg); 
                    res.redirect('/login'); 
                } else {
                    if(rows.length <=0) {
                        req.flash('msg_error', "Wrong email address or password. Try again."); 
                        res.redirect('/login');
                    }
                    else {   
                        session_store.is_login = true;
                        session_store.user = {
                        	id: rows[0].id,
                        	name: rows[0].name
                        }
                        res.redirect('/projects');
                    }
                }
 
            });
        });
    }
    else {
        res.redirect('/login'); 
    }
});
router.get('/logout', function(req, res)
{ 
    req.session.destroy(function(err)
    { 
        if(err)
        { 
            console.log(err); 
        } 
        else
        { 
            res.redirect('/login'); 
        } 
    }); 
});
module.exports = router;